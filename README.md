# Ecommer-Laravel-VueJS

## Participantes
1. **Hafid Oxte Dzul** 
    * ***Matricula***: 140300042
    * ***gitlab***: <https://gitlab.com/HafidOD>

2. **Brian Asael Noh Cocom**
    * ***Matricula***: 130300126
    * ***gitlab***: <https://gitlab.com/asaelNC>

3. **Luis Manuel Vargas Guerrero**
    * ***Matricula***: 140300057
    * ***gitlab***: <https://gitlab.com/LMVGUERRERO>


<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

